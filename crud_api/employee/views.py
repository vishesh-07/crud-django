from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Employee
from .serializer import EmployeeSerializer
from rest_framework import status
from django.http import Http404
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework import permissions,authentication
from rest_framework.pagination import PageNumberPagination
# Create your views here.
class EmployeeList(APIView,PageNumberPagination):
    page_size=3
    def get(self,request):
        name_filter=request.query_params.get('name')
        email_filter=request.query_params.get('email')
        if name_filter and email_filter:
            employee=Employee.objects.filter(name__iexact=name_filter,email__iexact=email_filter)
        elif name_filter:
            employee=Employee.objects.filter(name__iexact=name_filter)
        elif email_filter:
            employee=Employee.objects.filter(email__iexact=email_filter)
        else:
            employee=Employee.objects.all()
        employee=employee.order_by('name')
        paginated=self.paginate_queryset(employee,request)
        serializer=EmployeeSerializer(paginated,many=True)
        return self.get_paginated_response(serializer.data)
    def post(self,request):
        serializer=EmployeeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
class EmployeeDetail(APIView):
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = (permissions.IsAdminUser,)
    def get_permissions(self):
            if self.request.method == 'DELETE':
                return [permissions.IsAdminUser()]
            return [permissions.AllowAny()]
    def get_object(self,emp_id):
        try:
            return Employee.objects.get(pk=emp_id)
        except Exception as e:
            raise Http404
    def get_token(self,user):
        try:
            token,created=Token.objects.get_or_create(user=user)
            return token.key
        except Exception as e:
            raise Http404
    def get(self,request,emp_id):
        employee=self.get_object(emp_id)
        serializer=EmployeeSerializer(employee)
        return Response(serializer.data)
    def put(self,request,emp_id):
        employee=self.get_object(emp_id)
        serializer=EmployeeSerializer(employee,data=request.data,partial=True)
        if not request.data.get('emp_id')==None or not request.data.get('email')==None:
            return Response({'message':'You can not update Emp_id or Email'})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,emp_id):
        # user=User.objects.get(username='visheshsuryavanshi')
        # print("token is {}".format(self.get_token(user)))
        employee=self.get_object(emp_id)
        employee.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)