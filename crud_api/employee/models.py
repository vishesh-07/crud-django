from django.db import models
from phone_field import PhoneField
# Create your models here.
class Employee(models.Model):
    name=models.CharField(max_length=100)
    emp_id=models.IntegerField(primary_key=True)
    location=models.CharField(max_length=50)
    phone=models.CharField(max_length=12,unique=True)
    email=models.EmailField(unique=True)
    joining_date=models.DateField()
    def __str__(self):
        return self.name